package com.example.gamer.homework9chatusingfirebase.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.gamer.homework9chatusingfirebase.MessageActivity;
import com.example.gamer.homework9chatusingfirebase.R;
import com.example.gamer.homework9chatusingfirebase.model.UserData;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    private Context context;
    private List<UserData> userDataList;

    public UserAdapter(Context context,List<UserData> userDataList){
        this.context=context;
        this.userDataList=userDataList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

       View view=LayoutInflater.from(context).inflate(R.layout.show_users,viewGroup,false);
        return new UserAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final UserData userData=userDataList.get(i);
        myViewHolder.allUsername.setText(userData.getUsername());
        if (userData.getImageURL().equals("default"))
            myViewHolder.allUserImg.setImageResource(R.mipmap.ic_launcher);
        else
            Glide.with(context).load(userData.getImageURL()).into(myViewHolder.allUserImg);


        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context,MessageActivity.class);
                i.putExtra("userid",userData.getId());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userDataList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

       public TextView allUsername;
       public ImageView allUserImg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            allUsername=itemView.findViewById(R.id.txtShowAllUser);
            allUserImg=itemView.findViewById(R.id.imgShowUser);
        }
    }
}
