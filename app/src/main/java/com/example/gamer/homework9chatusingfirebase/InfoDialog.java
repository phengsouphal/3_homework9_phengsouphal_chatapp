package com.example.gamer.homework9chatusingfirebase;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class InfoDialog extends AppCompatDialogFragment {

    TextView txtinfo;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.layout_dialog,null);

        builder.setView(view)
                .setTitle("Information")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        txtinfo=view.findViewById(R.id.txtinfo);
        txtinfo.setText("Email::\n" +
                        "kakaka@gmail.com\n" +
                        "kaka1@gmail.com\n" +
                        "kaka2@gmail.com\n" +
                        "kaka3@gmail.com\n" +
                        "Password::" +
                        "123456789");
        return builder.create();
    }
}
