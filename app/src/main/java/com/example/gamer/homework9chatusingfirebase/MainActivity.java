package com.example.gamer.homework9chatusingfirebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.gamer.homework9chatusingfirebase.adapter.UserAdapter;
import com.example.gamer.homework9chatusingfirebase.model.UserData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    CircleImageView profileUser;
    TextView txtUsername;
    ImageView btnImageLogout;

    FirebaseUser firebaseUser;
    DatabaseReference databaseReference;

    UserAdapter userAdapter;
    RecyclerView recyclerView;
    List<UserData> userDataList;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        profileUser=findViewById(R.id.profileUserImg);
        txtUsername=findViewById(R.id.txtUsername);
        btnImageLogout=findViewById(R.id.btnlogout);
        recyclerView=findViewById(R.id.recyclerShowUsers);

        dialog=new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading...");
        dialog.show();


        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        userDataList=new ArrayList<>();
        getAllUsers();


        firebaseUser=FirebaseAuth.getInstance().getCurrentUser();
        databaseReference=FirebaseDatabase.getInstance().getReference("Users").child(firebaseUser.getUid());

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserData user=  dataSnapshot.getValue(UserData.class);
                 txtUsername.setText(user.getUsername());
                 Log.e("ooooo",txtUsername.toString());

                if (user.getImageURL().equals("default")){
                   profileUser.setImageResource(R.mipmap.ic_launcher_round);
                 }else
                     Glide.with(MainActivity.this).load(user.getImageURL()).into(profileUser);

                dialog.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        btnImageLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(MainActivity.this,StartActivity.class));
                finish();
            }
        });



    }


    private void getAllUsers(){
        final FirebaseUser firebaseUser=FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference=FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userDataList.clear();
                for (DataSnapshot snapshot:dataSnapshot.getChildren()){
                    UserData userData=snapshot.getValue(UserData.class);

                    assert userData!=null;
                    assert firebaseUser!=null;
                    if (!userData.getId().equals(firebaseUser.getUid())){
                        userDataList.add(userData);
                    }
                }
                userAdapter=new UserAdapter(MainActivity.this,userDataList);
                recyclerView.setAdapter(userAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
