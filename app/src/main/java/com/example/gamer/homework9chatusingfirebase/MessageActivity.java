package com.example.gamer.homework9chatusingfirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.gamer.homework9chatusingfirebase.adapter.MessageAdapter;
import com.example.gamer.homework9chatusingfirebase.model.Chat;
import com.example.gamer.homework9chatusingfirebase.model.UserData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageActivity extends AppCompatActivity {


    CircleImageView user_image;
    TextView username;

    FirebaseUser firebaseUser;
    DatabaseReference databaseReference;

    Intent intent;
    ImageButton btnimg;
    EditText edSendMess;


    MessageAdapter messageAdapter;
    RecyclerView recyclerView;
    List<Chat>  chatList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        android.support.v7.widget.Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        user_image=findViewById(R.id.profileUserImg);
        username=findViewById(R.id.txtUsername);
        btnimg=findViewById(R.id.btnSend);
        edSendMess=findViewById(R.id.edSendMess);
        recyclerView=findViewById(R.id.recyclerView);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(MessageActivity.this);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);


        intent=getIntent();

        final String userid=intent.getStringExtra("userid");

        firebaseUser=FirebaseAuth.getInstance().getCurrentUser();

        btnimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg=edSendMess.getText().toString();
                if (!msg.equals("")){
                    sendMessage(firebaseUser.getUid(),userid,msg);
                }else {
                    Toast.makeText(MessageActivity.this,"Input Cannot be Empty!!!",Toast.LENGTH_SHORT).show();
                }
                edSendMess.setText("");
            }
        });



        databaseReference=FirebaseDatabase.getInstance().getReference("Users").child(userid);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserData userData=dataSnapshot.getValue(UserData.class);
                username.setText(userData.getUsername());

                if (userData.getImageURL().equals("default"))
                    user_image.setImageResource(R.mipmap.ic_launcher);
                else
                    Glide.with(MessageActivity.this).load(userData.getImageURL()).into(user_image);

                sendMessages(firebaseUser.getUid(),userid,userData.getImageURL());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void sendMessage(String sender,String receiver,String message){
        DatabaseReference databaseReference=FirebaseDatabase.getInstance().getReference();

        HashMap<String,Object> hashMap=new HashMap<>();

        hashMap.put("sender",sender);
        hashMap.put("receiver",receiver);
        hashMap.put("message",message);

        databaseReference.child("Chats").push().setValue(hashMap);
    }


    private void sendMessages(final String myid, final String userid, final String imgUrl){

        chatList=new ArrayList<>();
        databaseReference=FirebaseDatabase.getInstance().getReference("Chats");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                chatList.clear();

                for (DataSnapshot snapshot:dataSnapshot.getChildren()){

                Chat chat=snapshot.getValue(Chat.class);
                if (chat.getReceiver().equals(myid)&&chat.getSender().equals(userid)
                        || chat.getReceiver().equals(userid)&&chat.getSender().equals(myid)){
                    chatList.add(chat);
                }
                messageAdapter=new MessageAdapter(MessageActivity.this,chatList,imgUrl);
                recyclerView.setAdapter(messageAdapter);
              }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }





}
